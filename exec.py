import pandas as pd
import numpy as np
ref = {
	'code' : '',
	'author' : '',
	'title' : '',
	'journal' : '',
	'year' : '',
	'volume' : '',
	'number' : '',
	'doi' : '',
	'art_number' : '',
	'pages' : '',
	'note' : '',
	'url' : '',
	'affiliation' : '',
	'abstract' : '',
	'keywords' : '',
	'references' : '',
	'publisher' : '',
	'issn' : '',
	'language' : '',
	'abbrev_source_title' : '',
	'document_type' : '',
	'source' : ''
}

ieee_types = {
	'IEEE Journals & Magazines' : 'ARTICLE',
	'IEEE Conferences' : 'INPROCEEDINGS',
	'IEEE Early Access Articles' : 'ARTICLE',
	'IET Journals & Magazines' : 'ARTICLE',
	'IEEE Standards' : 'ARTICLE',
	'IET Conferences': 'INPROCEEDINGS',
	'TUP Journals & Magazines' : 'ARTICLE'
}

def generate_bib(ref):
	for key, value in ref.items():
		if value == 'nan':
			ref[key]=''
	string = '@'
	string += ref['document_type'] + '{' + ref['code'] +',\n'
	string += 'author={' + ref['author'] + '},\n' 
	if ref['document_type'] == 'ARTICLE':
		string += 'journal={' + ref['journal'] + '},\n' 
	else:
		string += 'booktitle={' + ref['journal'] + '},\n' 	
	string += 'title={' + ref['title'] + '},\n' 
	string += 'year={' + ref['year'] + '},\n' 
	string += 'volume={' + ref['volume'] + '},\n' 
	string += 'number={' + ref['number'] + '},\n' 
	string += 'pages={' + ref['pages'] + '},\n' 
	string += 'abstract={' + ref['abstract'] + '},\n' 
	# string += 'author_keywords={' + ref['author_keywords'] + '},\n' 
	string += 'keywords={' + ref['keywords'] + '},\n' 
	string += 'doi={' + ref['doi'] + '},\n' 
	string += 'ISSN={' + ref['issn'] + '},\n'
	string += 'url={' + ref['url'] + '},\n' 
	string += 'publisher={' + ref['publisher'] + '},\n' 
	string += 'art_number={' + ref['art_number'] + '},\n' 
	string += 'note={' + ref['note'] + '},\n' 
	string += 'affiliation={' + ref['affiliation'] + '},\n' 
	string += 'references={' + ref['references'] + '},\n' 
	string += 'language={' + ref['language'] + '},\n' 
	string += 'abbrev_source_title={' + ref['abbrev_source_title'] + '},\n' 
	string += 'document_type={' + ref['document_type'] + '},\n' 
	string += 'source={' + ref['source'] + '},\n'
	string += 'month={},'
	string += '}'
	return string

def format_authors(authors):
	author_list = authors.split(';')  
	new_author_list = []
	for author in author_list:
		names = author.split(' ')
		last_name = names[-1]
		last_name = '{'+last_name+'}'
		names[-1] = last_name
		new_name = ''.join(name+' ' for name in names) #call string constructor and concat all elements in vector
		new_author_list.append(new_name)
	aux = ''.join(author+'and' for author in new_author_list[:-1])
	return aux + new_author_list[-1][:-1] #new_author_list[-1][:-1] remove extra space from the last author name 

def format_abstract(abstract):
	temp = abstract.split('@')
	aux = ''.join(x+'At' for x in temp)
	return aux

def main():
	f_in = 'ieee_2k.csv'
	f_out = 'test.bib'
	df = pd.read_csv(f_in)
	df = df.replace(np.nan, '', regex=True)

	# print(df.columns.values)
	# df = df.loc[:, ['Publication Title', 'Start Page', 'End Page', 'Author Affiliations', 'Authors', 'Document Title', 'Publication_Year', 'Volume', 'DOI', 'Document Identifier', 'Article Citation Count', 'PDF Link', 'Abstract', 'Author Keywords', 'Publisher', 'ISSN']]
	

	code = 1
	for index, row in df.iterrows():
		ref['code'] = str(code)
		ref['author'] = format_authors(str(row['Authors']))
		ref['title'] = str(row['Document Title'])
		ref['journal'] = str(row['Publication Title']) #bullshit
		ref['year'] = str(row['Publication_Year'])
		ref['volume'] = str(row['Volume']) if row['Volume'] != '' else ''
		ref['number'] = str(int(row['Issue'])) if row['Issue'] != '' else ''
		ref['doi'] = str(row['DOI'])
		# ref['art_number'] = str(0) #bullshit
		# ref['pages'] = str(int(row['Start Page'])) + '-' + str(int(row['End Page']))
		# ref['note'] = 'cited by '+str(row['Article Citation Count'])
		ref['url'] = str(row['PDF Link'])
		# ref['affiliation'] = str(row['Author Affiliations'])
		ref['abstract'] = str(format_abstract(row['Abstract']))
		keywords = str(row['INSPEC Controlled Terms']) +';'+ str(row['INSPEC Non-Controlled Terms']) +';'+ str(row['IEEE Terms']) +';'+ str(row['Author Keywords'])
		ref['keywords'] = keywords # wtf de campo é esse?
		# ref['references'] = '???' #IEEE nao exporta refs
		ref['publisher'] = str(row['Publisher'])
		issn = str(row['ISSN']).split(';')[0]
		ref['issn'] = issn
		# ref['language'] = 'English' #bullshit
		# ref['abbrev_source_title'] = '???'
		if row['Document Identifier'] == 'IEEE Standards':
			print(ref['url'])
		ref['document_type'] = ieee_types[row['Document Identifier']] #bullshit
		ref['source'] = 'IEEE' #bullshit
		# return

		bib_string = generate_bib(ref)
		print(bib_string)
		code += 1
	# print(df.columns.values)
	# print(df)
# , journal,, number, art_number,,, affiliation, ,, keywords, references,,, language, abbrev_source_title, document_type, source

main()
